#!/bin/bash


web_path=$1
current_path=$2
artifact=$3

cd $web_path
unzip ./$artifact
if [ $? -eq 0 ]
then
	if [ -e "$current_path/.env" ]
	then 
		rm $current_path/.env
		echo '.env was removed success'
	else echo '.env not found'
	fi
else 
	echo "unzip faild"
	exit 1
fi
if [ $? -eq 0 ]
then
	echo 'create .env link'
	ln -s $web_path/.env $current_path/.env
else
	echo "clean .env faild"	
	exit 1
fi	
cd $current_path
php artisan key:generate
php artisan aimeos:setup
php artisan migrate
if [ -e  $web_path/current/public/preview ]
then cp -r $web_path/current/public/preview $current_path/public && chmod 775 -R $current_path/public/preview
fi	
chown -R root:www-data $current_path
chmod -R 774 $current_path/storage/logs
if [ -L $web_path/current ]
then unlink $web_path/current
fi
ln -s $current_path $web_path/current 
echo "what else $?"

systemctl restart php7.2-fpm
rm $web_path/$artifact
