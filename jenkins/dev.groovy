#!groovy

def web_path = "/var/www/kirmanshop.loc"
def current_path = "${web_path}/${BUILD_NUMBER}-${BUILD_DATE}"
def artifact = "artifact-${BUILD_NUMBER}-${BUILD_DATE}.zip"
def node_name = "kirman-web"
def builds_path = "/opt/jenkins/workspace/${JOB_BASE_NAME}-builds"

pipeline {
    agent any
    options {
        timestamps()
    }
    triggers {
        bitBucketTrigger([[$class: 'BitBucketPPRRepositoryTriggerFilter', 
                            actionFilter: [$class: 'BitBucketPPRRepositoryPushActionFilter', 
                                allowedBranches: 'dev', 
                                triggerAlsoIfNothingChanged: true, 
                                triggerAlsoIfTagPush: false]
                        ]])
    }
    stages {
        stage('git checkout') {
            agent{
                node{
                    label "${node_name}"
                }
            }
            steps {
                git(
                    branch: 'dev', 
                    credentialsId: 'evalaser-bitbucket', 
                    url: 'git@bitbucket.org:ashuikam/kirmanshop.git'
                )
            }
        }
        stage('build') {
            agent{
                node{
                    label "${node_name}"

                }
            }
            steps {
                fileOperations([
                    folderCreateOperation("${current_path}/bootstrap/cache"), 
                    folderCreateOperation("${current_path}/storage/framework/cache/data"),
                    folderCreateOperation("${current_path}/storage/framework/views"),
                    folderCreateOperation("${current_path}/storage/framework/sessions"),
                    folderCreateOperation("${current_path}/storage/logs"),
                    folderCreateOperation("${builds_path}"),
                ])
                fileOperations([
                    fileCopyOperation(
                        excludes: '', 
                        flattenFiles: false, 
                        includes: '**', 
                        renameFiles: false, 
                        sourceCaptureExpression: '', 
                        targetLocation: "${current_path}", 
                        targetNameExpression: '**')])
                sh(
                    label: 'create symlink to .env', 
                    returnStdout: true, 
                    script: "ln -s ${web_path}/.env ${current_path}/.env"    
                )
                sh(
                    label: 'up project with composer', 
                    returnStdout: true, 
                    script: "cd ${current_path} && composer update"    
                )
                sh(
                    label: 'gen laravel key', 
                    returnStdout: true, 
                    script: "cd ${current_path} && php artisan key:generate"    
                )
                sh(
                    label: 'run publish', 
                    returnStdout: true, 
                    script: "cd ${current_path} && php artisan vendor:publish --all"    
                )
                sh(
                    label: 'run migrations', 
                    returnStdout: true, 
                    script: "cd ${current_path} && php artisan migrate"    
                )
                sh(
                    label: 'run setup', 
                    returnStdout: true, 
                    script: "cd ${current_path} && php artisan aimeos:setup"    
                )
                sh(
                    returnStdout: true,
                    script: "chmod -R 0775 ${current_path}/storage"
                )
                sh(
                    label: 'set permissions', 
                    returnStdout: true, 
                    script: "sudo chown -R root:www-data ${current_path}"    
                )
                sh(
                    label: 'set permissions', 
                    returnStdout: true, 
                    script: "sudo chmod -R 774 ${current_path}/public"    
                )
                sh(
                    label: 'create artifact', 
                    returnStdout: true, 
                    script: "cd ${web_path} && zip -r ${builds_path}/${artifact} ${BUILD_NUMBER}-${BUILD_DATE}"    
                )
                script {
                    try {
		                sh "[ ! -L '${web_path}/current' ]"
		            }
		            catch (exc) {
		                sh "unlink ${web_path}/current"
	                }
	                finally {
	                    sh(
                            label: 'switch to new version', 
                            returnStdout: true, 
                            script: "ln -s ${current_path} ${web_path}/current"    
                        )
	                }
                }
		    }
		    
		}
		stage('test') {
		    agent{
                node{
                    label "${node_name}"
                }
            }
		    steps{
		        echo 'test'
		    }
		}
    }
}
