#!bin/python3

# coded by Mikalai Ashuika
# e-mail ashuikam@gmail.com
#
# The script gets products and categories from planetakolyasok.by

import re
import json
import csv

import ssl
import requests

import pytils
from bs4 import BeautifulSoup
from symbol import except_clause

class SingleProduct:

    def __init__(self):
        self.user = ""
        self.passw = ""
        self.ownShop = 'https://malyshkin.by'
        self.localShop = 'https://malyshkin.by'
        self.request = ''
        self.url = 'https://planetakolyasok.by'
        self.get = ''
        self.props = []
        self.fieldnames = [
            'item code',
            'item label',
            'item type',
            'item status',
            'text type',
            'text content',
            'text type',
            'text content',
            'media url',
            'price currencyid',
            'price quantity',
            'price value',
            'price tax rate',
            'attribute code',
            'attribute type',
            'subproduct code',
            'product list type',
            'property value',
            'property type',
            'catalog code',
            'catalog list type'
        ]
        self.valid = [
            'Основные',
            'Размер и вес',
            'Рама и ходовая часть',
            'Люлька',
            'Другие'
        ]
        self.podcats = {
            'Коляски': 'kolyaski',
            'Универсальные коляски': 'universal_colyaski',
            'Классические коляски': 'classic_colyaski',
            'Прогулочные коляски': 'progulka_colyaski',
            'Коляски-трансформеры (Джипы)': 'transformers_colyaski',
            'Для тройни': '3xcolyaski',
            'Коляски для двойни': '2xcolyaski',
            'Стульчики для кормления': 'chairs',
            'Детская мебель': 'furniture',
            'Комоды': 'komodi',
            'Кроватки детские': 'krovatki',
            'Шкафы': 'shkafi',
            'Пеленальные столики': 'pelenalnie_stoliki',
            'Постельные принадлежности': 'mattresses',
            'Автокресла': 'auto_kresla',
            'Аксессуары': 'acsessuari',
        }

    def _startSession(self, session):
        self.request = session

    def startParse(self):
        self._startSession(requests.Session())
        self.setURI('/catalog/')
        soup = BeautifulSoup(self.getPage(), 'html.parser')
        if soup:
            catalog = self.getProducts(self.getCatalog(soup))
        for cat, podcats in catalog.items():
            try:
                for k, podcat in podcats.items():
                    for uri in podcat:
                        if uri:
                            self.csvPut(cat, self.getSingleProduct(uri, k))
            except AttributeError:
                print(k)
                print(podcat)
        for prop in self.props:
            self.saveNewAttrType(prop)
        self.csvPut('attributes', self.props)

    def csvPut(self, category, product):
        with open(category + '.csv', 'a', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(product)

    def getProducts(self, catalog):
        if catalog:
            for category,level1 in catalog.items():
                try:
                    # may be Pool
                    for podcat,link in level1.items():
                        catalog[category][podcat] = self.getProdLinks(link)
                except AttributeError:
                    catalog[category] = self.getProdLinks(level1)
        return catalog

    def getProdLinks(self, uri):
        links = []
        self.setURI(uri)
        soup = BeautifulSoup(self.getPage(), 'html.parser')
        while True:
            a = soup.findAll('a', {'class': 'product-item-image-wrapper'})
            for link in a:
                links.append(link.get('href'))
            try:
                self.setURI(soup.find('li', {'class':'bx-pag-next'}).find('a').get('href'))
            except:
                return links
            soup = BeautifulSoup(self.getPage(), 'html.parser')

    def getSingleProduct(self, link, podcat):
        try:
            product = []
            self.setURI(link)
            soup = BeautifulSoup(self.getPage(), 'html.parser')
            # name block start
            product.append(self.getName(soup))
            product.append(self.getName(soup))
            # name block end
            product.append('default')
            product.append('0')
            product.append('short')
            product.append(self.getShortDescription(soup))
            product.append('long')
            product.append(self.getDescription(soup))
            product.append('media/url.img')
            product.append('BYN')
            product.append('1')
            product.append('1000.00')
            product.append('')
            #specification block end
            prop = self.getSpecification(soup)
            self._uniquieAtrributes(prop)
            for i in range(60):
                try:
                    if i % 2 == 0:
                        product.append(self._translateAttr(prop[i]))
                    else:
                        product.append('' + prop[i])
                except IndexError:
                    product.append('')
            # specification block end
            product.append('')
            product.append('default')
            product.append('')
            product.append('')
            # catalog code
            product.append(self._translateCatalogCode(podcat))
            product.append('')
            return product
        except Exception as error:
            print(error)

    def _uniquieAtrributes(self, props):
        for i in range(len(props)):
            if i % 2 == 0 and not props[i] in self.props:
                self.props.append(props[i])


    def _translateCatalogCode(self, podcat):
        try:
            return self.podcats[podcat]
        except IndexError:
            return 'default'
        except Exception as error:
            print(error)
            return 'default'

    def getPage(self, url=False):
        try:
            if not url:
                url = self.url
            response = self.request.get(url + self.get)
            if response.status_code == 200:
                return response.text
            else:
                raise Exception("No server's data, code response: %s"
                                % response.status_code)
        except Exception as error:
            print(error)
            print(self.get)
            return False

    def getJsonFromShop(self):
        try:
            response = self.request.get(self.localShop + self.get)
            if response.status_code == 200:
                return response.json()
            else:
                raise Exception("No server's data, code response: %s"
                                % response.status_code)
        except Exception as error:
            print(error)
            print(self.get)
            return False

    def postJson(self, json):
        try:
            headers = {"Content-Type": "application/json"}
            response = self.request.post(self.localShop + self.get, headers = headers, json = json )
            if response.status_code == 200:
                return response.json()
            else:
                raise Exception("No server's data, code response: %s"
                                % response.status_code)
        except Exception as error:
            print(error)
            print(self.get)
            return False

    def saveNewAttrType(self, newAttr):
        self._startSession(requests.Session())
        try:
            if not self.authShop():
                raise Exception("I can't auth")
            token = self.checkAttrsTypesFromShop(newAttr)
            if token:
                self.setURI('/admin/default/jsonadm/attribute/type?_token={}'.format(token))
                json = {"data": {
                            "type": "attribute/type",
                            "attributes": {
                                "attribute.type.domain": "product",
                                "attribute.type.label": newAttr,
                                "attribute.type.status": 1,
                                "attribute.type.code": self._translateAttr(newAttr)
                            }
                        }
                    }
                self.postJson(json)
        except Exception as error:
            print(error)


    def checkAttrsTypesFromShop(self, newAttr):
        uri = '/admin/default/jsonadm/attribute/type'
        self.setURI(uri)
        json = self.getJsonFromShop()
        for attr in json['data']:
            if newAttr == attr['attributes']['attribute.type.code']:
                return False
        return json['meta']['csrf']['value']


    def authShop(self):
        try:
            headers = {"Content-Type": "application/x-www-form-urlencoded"}
            uri = "/login"
            post = {'email': self.user, 'password': self.passw, '_token': self._getToken(uri)}
            response = self.request.post(self.localShop + self.get, headers=headers, data=post)
            if response.status_code != 200:
                raise Exception("No server's data, code response: %s"
                                % response.status_code)
            return True
        except Exception as error:
            print(error)
            # print(self.get)
            return False

    def _getToken(self, uri):
        try:
            self.setURI(uri)
            soup = BeautifulSoup(self.getPage(self.localShop), 'html.parser')
            token = soup.find('input', {'name': '_token'}).get('value')
            return token
        except Exception as error:
            print(error)

    def getDescription(self, soup):
        try:
            return self._clear(soup.find('div', {'data-value': 'description'}).text)
        except AttributeError:
            return ''
        except Exception as error:
            print(error)
            return False

    def getSpecification(self, soup):
        try:
            properties = {}
            soup_properties = soup.find('div', {'data-value': 'properties'})
            for fatal_prop in soup_properties.find_all('div', {'class': 'fatal_prop'}):
                divs = fatal_prop.find('div', {'class': 'flex_product_properties'}).find_all('div')
                for i in range(len(divs)):
                    if divs[i]['class'][0] == 'name_properties':
                        # changing code start
                        key = self._clear(self._clear(fatal_prop.find('div', {'class': 'main_detail_prop'}).text))
                        if not key in properties:
                            properties[key] = {}
                        properties[key].update({self._clear(divs[i].text): self._clear(divs[i+1].text)})
            return properties
        except AttributeError:
            return ''
        except Exception as error:
            print(error)
            return False

    def _clear(self, str):
        if str:
            return re.sub(r'^\s+|\n|\r|\s+$', '', str)
        else:
            return str

    def _translateAttr(self, attr, write=True):
        if len(attr) >= 32 :
            attr_translate = pytils.translit.translify(attr)[:32]
            if write:
                self.csvPut('translate_attrs', [attr_translate, attr])
            return attr_translate
        else:
            return attr

    def getShortDescription(self, soup):
        try:
            return self._clear(soup.find('span', {'class': 'brandblock-text'}).text)
        except AttributeError:
            return ''
        except Exception as error:
            print(error)
            return False

    def getName(self, soup):
        try:
            return self._clear(soup.find('h1', {'class': 'h1-title'}).text)
        except Exception as error:
            print(error)
            print('line 181')
            return False

    def setURI(self, uri):
        if uri:
            self.get = uri

    def getCatalog(self, soup):
        catalog = {}
        menu_catalog = soup.find('div', {'class': 'bx_sitemap'}).find('ul')
        new_li = soup.new_tag('li')
        menu_catalog.insert(0, new_li)
        first_li = menu_catalog.find('li')
        collect = first_li.findNextSiblings('li')
        for li in collect:
            try:
                title = li.find('span', {'class': 'bx_sitemap_li_title'}). \
                    find('div', {'class': 'catalog_menu_title'}).text
                level1 = {}
                ul = li.find('ul', {'class': 'menu_level1'})
                iter(ul)
                a = ul.find_all('a')
                for link in a:
                    level1[link.find('img').get('title')] = link.get('href')
                catalog[title] = level1
            except TypeError:
                catalog[title] = li.find('a').get('href')
        return catalog


class ParseCategory(SingleProduct):

    def getSpecification(self, soup):
        try:
            properties = []
            soup_properties = soup.find('div', {'data-value': 'properties'})
            for fatal_prop in soup_properties.find_all('div', {'class': 'fatal_prop'}):
                divs = fatal_prop.find('div', {'class': 'flex_product_properties'}).find_all('div')
                for i in range(len(divs)):
                    if divs[i]['class'][0] == 'name_properties':
                        # changing code start
                        key = self._clear(self._clear(fatal_prop.find('div', {'class': 'main_detail_prop'}).text))
                        if key in self.valid:
                            properties.append(self._clear(divs[i].text))
                            properties.append(self._clear(divs[i+1].text))
            return properties
        except AttributeError:
            return ''
        except Exception as error:
            print(error)
            return False

    def getOutCatalog(self):
        print(json.dumps(self.getCatalog()))

    def getOutProduct(self):
        print(json.dumps(self.product))


def main():
    parser = ParseCategory()
    parser.startParse()
    # parser.saveNewAttrType('Перекидная ручка')
if __name__ == "__main__":
    main()
